<?php include_once('functions/functions.php'); ?>
<!doctype html>
<html lang="en">
	
	<!-- HEAD -->
	<head>
		<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PandoraLab</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" href="css/main.css" type="text/css" >
    </head>
	
	<!-- BODY -->
    <body>
	
		<!-- BODY CONTAINER -->
		<div class="container">
		
			<!-- HEADER -->
			<header>
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">  
						<img src="images/Banner.jpg" alt="Toi Ohomai Banner">
					</div>
				</div>
			</header>
		
			<!-- NAV -->
			<div class="row" id="navrow">   
                <div class="col-sm-10 col-sm-offset-1" id="nopaddingtop"> 
					<nav class="navbar">
					
						<!-- NAV HEADER -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="index.php">PandoraLab</a>
						</div>
						
						<!-- NAV LINKS -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav navbar-right">
								<li class="active"><a href="index.php">HOME</a></li>
								<li><a href="pandora.html">PANDORA</a></li>
								<li><a href="links.html">LINKS</a></li>
								<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">SETUP<span class="caret"></span></a>
									<ul class="dropdown-menu">
										<li><a href="googleapps.html">GOOGLE APPS</a></li>
										<li><a href="slack.html">SLACK</a></li>
										<li><a href="microsoftimagine.html">MICROSOFT IMAGINE</a></li>
									</ul>
								</li>
                                <li><a href="login.php">Login</a></li>
							</ul>
						</div>
					</nav>
				</div>
			</div>
		
			<!-- MAIN BODY-->
			<div class="row" id="outerbodybg">   
				<div class="col-sm-10 col-sm-offset-1" id="bodybg"> 
				
					<img src="images/Pandora.png" id="marginbottom" alt="Pandora Banner">
			
					<!-- php load -->
					<?php
					$page = "index";
					echo show_all_items(get_all_items($page));
					?>
				
					<!-- QUESTIONS NOTICE-->
					<h3>Any Questions?</h3>
					<p>If you have any questions, ask any of the IT staff and we can help you out.</p>
					<p>You can also post your questions in Slack, in the #general channel, and other students can help out as well!</p>
				</div>
			</div>
			
			<!-- FOOTER -->
			<footer>
				<div class="row">   
					<div class="col-sm-10 col-sm-offset-1">
						<div class="row">
							<div class="col-sm-6">  
								<img src="images/ToiOhomai.png" alt="Toi Ohomai Logo">
							</div>
							<div class="col-sm-6">  
								<img src="images/Waikato.gif" alt="Waikato Uni Logo">
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>
		
		<!-- SCRIPTS -->
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc=" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script>
			
		</script>
    </body>
</html>