<?php include_once('functions/functions.php'); 
session_start();
login(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" href="css/main.css" type="text/css" >
    </head>
    <body class="backing">
        <?php
        if( !isset($_SESSION['login']) )
        {
        ?>
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-header extraPadding">
                        <h2>Please enter in your credentials to login:</h2>
                    </div>
                    <div class="panel-body">
                        <form method="POST">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Username</span>
                                <input type="text" class="form-control" name="username" placeholder="Username" aria-describedby="basic-addon1">
                            </div>
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1">Password</span>
                                <input type="password" class="form-control" name="password" placeholder="Password" aria-describedby="basic-addon1">
                            </div>
                            <br>
                            <button type="submit" name="login" class="btn btn-success">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php
        }
        ?>        
    </body>
</html>