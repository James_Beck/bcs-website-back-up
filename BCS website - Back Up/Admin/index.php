<?php include_once('../functions/functions.php');
session_start();
logout(); ?>
<!DOCTYPE html>
<html>
    <body>
        <p>I can see this text because I am on the Administrators Page</p>

        <?php
        if( !isset($_SESSION['logout']) )
        {
        ?>
        <form method="POST">
            <button type="submit" name="logout" class="btn btn-success">Logout</button>
        </form>
        <?php
        }
        ?>
    </body>
</html>