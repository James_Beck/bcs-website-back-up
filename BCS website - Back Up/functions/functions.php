<?php
//Sets definition for the timezone
date_default_timezone_set('Pacific/Auckland');

//Define values for the database
define("DBHOST", "localhost");
define("DBUSER", "root");
define("DBPASS", "root");
define("DBNAME", "test");

//function that creates a connection to the database - Useful for other functions
function connect() {  

    $conn = new mysqli(DBHOST, DBUSER, DBPASS, DBNAME);
    
    if ($conn->connect_errno > 0) {
        die('Unable to connect to database ['.$conn->connect_errno.']');
    }
    
    return $conn;
}

//Webpage redirect
function redirect($url) {

    ob_start();
    header('Location: '.$url);
    ob_end_flush();
    die();
    
}

//Admin login
function login() {

    if(isset($_POST['login']))
    {
        $db = connect();

        $user = $db->real_escape_string($_POST['username']);
        $pass = $db->real_escape_string($_POST['password']);

        $sql = "SELECT * FROM tbl_admin WHERE usrnm = '$user' && psswrd = '$pass'";
        $arr = [];

        $result = $db->query($sql);

        if(!$result)
        {
            die("There was an error running the query [".$db->error."] ");
        }
        
        while ($row = $result->fetch_assoc())
        {
            $arr[] = array (
                "user" => $row['usrnm'],
                "pass" => $row['psswrd']
            );
        }

        $result->free();
        $db->close();

        if (($user == $arr[0]['user']) && ($pass == $arr[0]['pass']))
        {
            $_SESSION['login'] = TRUE;
            redirect("admin/index.php");
        }
        else
        {
            echo "<h1 class='removeSure'>Your login details are incorrect.</h1>";
            redirect("index.php");
        }
    }
}

//Logout when logged in as administrator
function logout()
{
    if(isset($_POST['logout']))
    {
        session_start();

        // Unset all of the session variables.
        $_SESSION = array();

        // If it's desired to kill the session, also delete the session cookie.
        // Note: This will destroy the session, and not just the session data!

        if (ini_get("session.use_cookies")) {
            $params = session_get_cookie_params();
            setcookie(session_name(), '', time() - 42000,
                $params["path"], $params["domain"],
                $params["secure"], $params["httponly"]
            );
        }

        // Finally, destroy the session.
        session_destroy();

        redirect("../index.php");
    }
}

//puts data from the database, filtered by relevant title to the index page
function get_all_items($received) {
    
    $db = connect();
    $sql = "SELECT * FROM tbl_values WHERE page = '$received'";
    $arr = [];
    
    $result = $db->query($sql);
    
    if(!$result) {
        die("There was an error running the query [".$db->error."] ");
    }
    while ($row = $result->fetch_assoc()) {
        $arr[] = array (
            "id" => $row['id'],
            "category" => $row['category'],
            "page" => $row['page'],
            "result" => $row['result']
        );
    }
    
    $json = json_encode($arr);
    
    $result->free();
    $db->close();
    
    return $json;
}

//Shows the relevant data for a page - with additions for administrator login
function show_all_items($data) {
    
    $array = json_decode($data, True);    
    $output = "";

    if (count($array) > 0 )
    {        
        for ($i = 0; $i < count($array); $i++)
        {           
            $output .= $array[$i]['result'];
        }
        return $output;               
    }
    else {
        $output .= "<tr><td colspan='5'>No Data Available</td></tr>";        
        return $output;
    }
}
?>